//
if (process.env.NODE_ENV !== 'development') {
    const hacks = require('viewport-units-buggyfill/viewport-units-buggyfill.hacks');
    const viewportUnitsBuggyfill = require('viewport-units-buggyfill');
    viewportUnitsBuggyfill.init({
        hacks,
        force: true
    });
}


// setInterval(() => {
//     viewportUnitsBuggyfill.refresh();
// }, 2000);



