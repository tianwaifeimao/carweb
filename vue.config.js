module.exports = {
  publicPath: '/dist/',

  css: {
    loaderOptions: {
      stylus: {
        'resolve url': true,
        'import': [
          './src/theme'
        ]
      }
    }
  },

  devServer: {
    proxy: {
      '/site': {
        target: 'http://www.cheyuanbaojia.com/',
        // target: 'http://cyw.haitow.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/site': ''
        }
      }
    }
  },

  pluginOptions: {
    'cube-ui': {
      postCompile: true,
      theme: true
    }
  }
}
