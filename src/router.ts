import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Car from './views/Car.vue';
import Carused from './views/Respect.vue';
import Mine from './views/Mine.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: () => import('./views/Index.vue'),
      children: [
        {
          path: '/',
          name: 'home',
          component: Home,
          meta: {
            keepAlive: true,
          },
        },
        {
          path: '/car',
          name: 'car',
          component: Car,
          meta: {
            keepAlive: true,
            isBack: true,
          },
        },
        {
          path: '/carused',
          name: 'carused',
          component: Carused,
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine,
        },
      ],
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('./views/Register.vue'),
    },
    {
      path: '/cardealer',
      name: 'cardealer',
      component: () => import('./views/Cardealer.vue'),
    },
    {
      path: '/article',
      name: 'article',
      component: () => import('./views/Article.vue'),
    },
    {
      path: '/articleadd',
      name: 'articleadd',
      component: () => import('./views/Articleadd.vue'),
    },
    {
      path: '/articledetail',
      name: 'articledetail',
      component: () => import('./views/Articledetail.vue'),
    },
    {
      path: '/history',
      name: 'history',
      component: () => import('./views/History.vue'),
    },
    {
      path: '/collect',
      name: 'collect',
      component: () => import('./views/Collect.vue'),
    },
    {
      path: '/cardetail',
      name: 'cardetail',
      component: () => import('./views/Cardetail.vue'),
      // meta:{
      //     keepAlive:true,
      // }
    },
    {
      path: '/letter',
      name: 'letter',
      component: () => import('./views/Letter.vue'),
    },
    {
      path: '/order',
      name: 'order',
      component: () => import('./views/Order.vue'),
    },
    {
      path: '/orderdetail',
      name: 'orderdetail',
      component: () => import('./views/Orderdetail.vue'),
    },
    {
      path: '/bindmobile',
      name: 'bindmobile',
      component: () => import('./views/Bindmobile.vue'),
    },
    {
      path: '/caradd',
      name: 'caradd',
      component: () => import('./views/Caradd.vue'),
    },
    {
      path: '/help',
      name: 'help',
      component: () => import('./views/Help.vue'),
    },
    {
      path: '/mycar',
      name: 'mycar',
      component: () => import('./views/Mycar.vue'),
    },
    {
      path: '/mycaradd',
      name: 'mycaradd',
      component: () => import('./views/Mycaradd.vue'),
    },
    {
      path: '/mycarview',
      name: 'mycarview',
      component: () => import('./views/Mycarview.vue'),
    },
    {
      path: '/respect',
      name: 'respect',
      component: () => import('./views/Respect.vue'),
    },
  ],
});
