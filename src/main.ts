import Vue from 'vue';
import './cube-ui';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import './js/viewport-units-buggyfill';
import Component from 'vue-class-component';


Vue.config.productionTip = false;

router.afterEach((to: any, from: any) => {
  setTimeout(() => {
    const storeState: any = store.state;
    // require('viewport-units-buggyfill').refresh();
    if (process.env.NODE_ENV !== 'development') {
      require('viewport-units-buggyfill').refresh();
    }
  }, 0);
});


// Component.registerHooks([
//   'beforeRouteEnter',
//   'beforeRouteLeave',
//   'beforeRouteUpdate',
// ]);


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

